from datetime import datetime

from fastapi import FastAPI
from pydantic import BaseModel

from db import async_session
from db.models import CreateUserCommand
from dal.user_dal import UserDAL


class MailingList(BaseModel):
    id: int
    started_at: datetime
    message_text: str
    filter: str
    ended_at: datetime


class Client(BaseModel):
    id: int
    phone_number: str
    mobile_operator_code: str
    tag: str
    timezone: str


class Message(BaseModel):
    id: int
    created_at: datetime
    status: str
    mailing_list_id: int
    client_id: int


app = FastAPI()


@app.post("/users")
async def create_user(
        cmd: CreateUserCommand
):
    async with async_session() as session:
        async with session.begin():
            user_dal = UserDAL(session)
            return await user_dal.create_user(cmd=cmd)
#
# @app.get("/books")
# async def get_all_books() -> List[Book]:
#     async with async_session() as session:
#         async with session.begin():
#             book_dal = BookDAL(session)
#             return await book_dal.get_all_books()
#
# @app.put("/books/{book_id}")
# async def update_book(book_id: int, name: Optional[str] = None, author: Optional[str] = None, release_year: Optional[int] = None):
#     async with async_session() as session:
#         async with session.begin():
#             book_dal = BookDAL(session)
#             return await book_dal.update_book(book_id, name, author, release_year)


# @app.post("/clients/")
# async def create_client(client: Client):
#     return client
#
#
# @app.patch("/clients/{client_id}")
# async def update_client(client: Client):
#     return client
#
#
# @app.delete("/clients/{client_id}")
# async def remove_client(client_id: Client.id):
#     return {"message": "client"}
#
#
# @app.post("/mail_lists/")
# async def create_mail_list(mail_lists: List[MailingList]):
#     return mail_lists
#
#
# @app.get("/mail_lists_statistics")
# async def get_mail_lists_statistics():
#     return {"message": "mail_lists_statistics"}
#
#
# @app.get("/mail_list_statistics/{mail_list_id}")
# async def get_detail_mail_list_statistics():
#     return {"message": "mail_lists_statistics"}
#
#
# @app.patch("/mail_lists/{mail_list_id}")
# async def patch_client(mail_list_id: MailingList.id):
#     return {"message": "mail_list"}
#
#
# @app.delete("/mail_lists/{mail_list_id}")
# async def remove_mail_list(mail_list_id: MailingList.id):
#     return {"message": "client"}
#
#
# @app.get("/active_mail_lists")
# async def send_messages_all_active_lists(mail_list_id: MailingList.id):
#     return {"message": "client"}
